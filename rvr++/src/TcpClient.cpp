//======================================================================================================================
// 2021 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//       File: TcpClient.h
//
//     Author: rmerriam
//
//    Created: Jun 27, 2021
//
//======================================================================================================================
#include <TcpClient.h>
#include <Trace.h>

//----------------------------------------------------------------------------------------------------------------------
namespace rvr {

    TcpClient::TcpClient(std::string const& addr, std::string const& port) :    //
        mAddress { addr }, mPort { port } {

        addrinfo* result;
        int s = getaddrinfo(mAddress.c_str(), mPort.c_str(), &mHints, &result);

        if (s != 0) {
            mIsOpen = false;
        }

        mys::tout << code_line << mIsOpen << mys::sp << result->ai_canonname;
        for (auto rp { result }; rp != NULL; rp = rp->ai_next) {

            mCommId = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
            mys::tout << code_line << mCommId;

            if (mCommId == -1) {
                continue;
            }

            mys::tout << code_line << mCommId << mys::sp << rp->ai_addr;

            if (connect(mCommId, rp->ai_addr, rp->ai_addrlen) == 0) {
                mIsOpen = true;
                mys::tout << code_line << mIsOpen << mys::sp << result->ai_canonname;
                break; /* Success */
            }
        }
        freeaddrinfo(result);

        mys::tout << code_line << mCommId;
    }
}
