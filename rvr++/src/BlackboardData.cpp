//======================================================================================================================
// 2021 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//		 File: BlackboardInformation.cpp
//
//     Author: rmerriam
//
//    Created: Nov 30, 2021
//
//======================================================================================================================

#include <Blackboard.h>

#include <ApiShell.h>
#include <Drive.h>
#include <IoLed.h>
#include <Power.h>
#include <SensorsDirect.h>
#include <SensorsStream.h>

namespace rvr {
    using dev = Devices;
    using bb = Blackboard;
    using sen_token = SensorsStream::Tokens;
    using sen_d = SensorsDirect;
    using drv = Drive;
//----------------------------------------------------------------------------------------------------------------------
    Blackboard::BBDictionary Blackboard::mDictionary {                                   //
    { bb::entryKey(bluetoothSOC, dev::api_and_shell, 0x00), bb::BlackboardEntry { "bt_echo" } },                          //
        { bb::entryKey(nordic, dev::api_and_shell, 0x00), bb::BlackboardEntry { "nordic_echo" } },                                //
        //
        { bb::entryKey(bluetoothSOC, dev::system, 0x00), bb::BlackboardEntry { "bt_main_application_version" } },      //
        { bb::entryKey(nordic, dev::system, 0x00), bb::BlackboardEntry { "nordic_main_application_version" } },                //
        { bb::entryKey(bluetoothSOC, dev::system, 0x01), bb::BlackboardEntry { "bt_bootloader_version" } },            //
        { bb::entryKey(nordic, dev::system, 0x01), bb::BlackboardEntry { "nordic_bootloader_version" } },                      //
        { bb::entryKey(nordic, dev::system, 0x03), bb::BlackboardEntry { "board_revision" } },                             //
        { bb::entryKey(nordic, dev::system, 0x06), bb::BlackboardEntry { "mac_address" } },                                //
        { bb::entryKey(nordic, dev::system, 0x13), bb::BlackboardEntry { "stats_id" } },                                   //
        { bb::entryKey(bluetoothSOC, dev::system, 0x1F), bb::BlackboardEntry { "bt_processor_name" } },                //
        { bb::entryKey(nordic, dev::system, 0x1F), bb::BlackboardEntry { "nordic_processor_name" } },                          //
        { bb::entryKey(nordic, dev::system, 0x38), bb::BlackboardEntry { "get_sku" } },                                   //
        { bb::entryKey(bluetoothSOC, dev::system, 0x39), bb::BlackboardEntry { "get_core_up_time_in_milliseconds" } },    //        //
        { bb::entryKey(nordic, dev::connection, 0x05), bb::BlackboardEntry { "get_bluetooth_advertising_name" } },         //
        //
        { bb::entryKey(nordic, dev::io_led, 0x1A), bb::BlackboardEntry { "set_all_leds" } },                               //
        { bb::entryKey(nordic, dev::io_led, 0x4E), bb::BlackboardEntry { "release_led_requests" } },                       //
        //
        { bb::entryKey(nordic, dev::power, 0x00), bb::BlackboardEntry { "power off" } },                                   //
        { bb::entryKey(nordic, dev::power, 0x01), bb::BlackboardEntry { "snooze" } },
        //
        { bb::entryKey(nordic, dev::power, 0x0D), bb::BlackboardEntry { "wake" } },                                   //
        { bb::entryKey(nordic, dev::power, 0x10), bb::BlackboardEntry { "get_battery_percentage" } },                      //
        { bb::entryKey(nordic, dev::power, 0x11), bb::BlackboardEntry { "system_awake_notify" } },                   //
        { bb::entryKey(nordic, dev::power, 0x17), bb::BlackboardEntry { "get_battery_voltage_state" } },                   //
        { bb::entryKey(nordic, dev::power, 0x19), bb::BlackboardEntry { "will_sleep_notify" } },                           //
        { bb::entryKey(nordic, dev::power, 0x1A), bb::BlackboardEntry { "did_sleep_notify" } },                            //
        { bb::entryKey(nordic, dev::power, 0x1B), bb::BlackboardEntry { "enable_battery_voltage_state_change_notify" } },    //
        { bb::entryKey(nordic, dev::power, 0x1C), bb::BlackboardEntry { "battery_voltage_state_change_notify" } },         //
        { bb::entryKey(nordic, dev::power, 0x25, Power::CalibratedFiltered), bb::BlackboardEntry { "get_battery_voltage_in_volts" } },        //
        { bb::entryKey(nordic, dev::power, 0x25, Power::CalibratedUnfiltered), bb::BlackboardEntry { "get_battery_voltage_in_volts" } },      //
        { bb::entryKey(nordic, dev::power, 0x25, Power::UncalibratedUnfiltered), bb::BlackboardEntry { "get_battery_voltage_in_volts" } },    //
        { bb::entryKey(nordic, dev::power, 0x26), bb::BlackboardEntry { "get_battery_voltage_state_thresholds" } },        //
        { bb::entryKey(bluetoothSOC, dev::power, 0x27), bb::BlackboardEntry { "get_current_sense_amplifier_current left" } },    //
        { bb::entryKey(bluetoothSOC, dev::power, 0x27, 1), bb::BlackboardEntry { "get_current_sense_amplifier_current right" } },             //
        //
        { bb::entryKey(bluetoothSOC, dev::drive, 0x01), bb::BlackboardEntry { "raw_motors" } },                            //
        { bb::entryKey(bluetoothSOC, dev::drive, 0x06), bb::BlackboardEntry { "reset_yaw" } },                             //
        { bb::entryKey(bluetoothSOC, dev::drive, 0x07), bb::BlackboardEntry { "drive_with_heading" } },                    //
        { bb::entryKey(bluetoothSOC, dev::drive, 0x0E), bb::BlackboardEntry { "set_default_control_system_for_type" } },                    //
        { bb::entryKey(bluetoothSOC, dev::drive, 0x22), bb::BlackboardEntry { "set_custom_control_system_timeout" } },             //
        { bb::entryKey(bluetoothSOC, dev::drive, 0x25), bb::BlackboardEntry { "enable_motor_stall_notify" } },             //
        { bb::entryKey(bluetoothSOC, dev::drive, 0x26), bb::BlackboardEntry { "motor_stall_notify" } },                    //
        { bb::entryKey(bluetoothSOC, dev::drive, 0x27), bb::BlackboardEntry { "enable_motor_fault_notify" } },             //
        { bb::entryKey(bluetoothSOC, dev::drive, 0x28), bb::BlackboardEntry { "motor_fault_notify" } },                    //
        { bb::entryKey(bluetoothSOC, dev::drive, 0x29), bb::BlackboardEntry { "get_motor_fault_state" } },                 //
        { bb::entryKey(bluetoothSOC, dev::drive, 0x32), bb::BlackboardEntry { "drive_tank_si_units" } },    //
        { bb::entryKey(bluetoothSOC, dev::drive, 0x33), bb::BlackboardEntry { "drive_tank_normalized" } },    //
        { bb::entryKey(bluetoothSOC, dev::drive, 0x34), bb::BlackboardEntry { "drive_rc_si_units" } },    //
        { bb::entryKey(bluetoothSOC, dev::drive, 0x35), bb::BlackboardEntry { "drive_rc_normalized" } },    //
        { bb::entryKey(bluetoothSOC, dev::drive, 0x36), bb::BlackboardEntry { "drive_with_yaw_si" } },    //
        { bb::entryKey(bluetoothSOC, dev::drive, 0x37), bb::BlackboardEntry { "drive_with_yaw_normalized" } },    //
        { bb::entryKey(bluetoothSOC, dev::drive, 0x38), bb::BlackboardEntry { "drive_to_position_si" } },    //
        { bb::entryKey(bluetoothSOC, dev::drive, 0x39), bb::BlackboardEntry { "drive_to_position_normalized" } },    //
        { bb::entryKey(bluetoothSOC, dev::drive, 0x3A), bb::BlackboardEntry { "xy_position_drive_result_notify" } },    //
        { bb::entryKey(bluetoothSOC, dev::drive, 0x3C), bb::BlackboardEntry { "set_drive_target_slew_parameters" } },    //
        { bb::entryKey(bluetoothSOC, dev::drive, 0x3D), bb::BlackboardEntry { "get_drive_target_slew_parameters" } },    //
        { bb::entryKey(bluetoothSOC, dev::drive, 0x3E), bb::BlackboardEntry { "drive_stop_custom_decel" } },    //
        { bb::entryKey(bluetoothSOC, dev::drive, 0x3F), bb::BlackboardEntry { "robot_has_stopped_notify" } },    //
        { bb::entryKey(bluetoothSOC, dev::drive, 0x40), bb::BlackboardEntry { "restore_default_drive_target_slew_parameters" } },    //
        { bb::entryKey(bluetoothSOC, dev::drive, 0x41), bb::BlackboardEntry { "get_stop_controller_state" } },    //
        { bb::entryKey(bluetoothSOC, dev::drive, 0x42), bb::BlackboardEntry { "drive_stop" } },    //
        { bb::entryKey(bluetoothSOC, dev::drive, 0x43), bb::BlackboardEntry { "restore_default_control_system_timeout" } },    //
        { bb::entryKey(bluetoothSOC, dev::drive, 0x44), bb::BlackboardEntry { "get_active_control_system_id" } },    //
        { bb::entryKey(bluetoothSOC, dev::drive, 0x45), bb::BlackboardEntry { "restore_initial_default_control_systems" } },    //
        { bb::entryKey(bluetoothSOC, dev::drive, 0x46), bb::BlackboardEntry { "get_default_control_system_for_type" } },    //
        //
        { bb::entryKey(bluetoothSOC, dev::sensors, 0x0F), bb::BlackboardEntry { "enable_gyro_max_notify" } },    //
        { bb::entryKey(bluetoothSOC, dev::sensors, 0x10), bb::BlackboardEntry { "gyro_max_notify" } },    //
        { bb::entryKey(bluetoothSOC, dev::sensors, 0x13), bb::BlackboardEntry { "reset_locator_x_and_y" } },    //
        { bb::entryKey(bluetoothSOC, dev::sensors, 0x17), bb::BlackboardEntry { "set_locator_flags " } },    //
        { bb::entryKey(bluetoothSOC, dev::sensors, 0x22), bb::BlackboardEntry { "get_bot_to_bot_infrared_readings " } },    //
        { bb::entryKey(nordic, dev::sensors, 0x23), bb::BlackboardEntry { "get_rgbc_sensor_values" } },    //
        { bb::entryKey(bluetoothSOC, dev::sensors, 0x25), bb::BlackboardEntry { "magnetometer_calibrate_to_north" } },    //
        { bb::entryKey(bluetoothSOC, dev::sensors, 0x27), bb::BlackboardEntry { "start_robot_to_robot_infrared_broadcasting" } },    //
        { bb::entryKey(bluetoothSOC, dev::sensors, 0x28), bb::BlackboardEntry { "start_robot_to_robot_infrared_following" } },    //
        { bb::entryKey(bluetoothSOC, dev::sensors, 0x29), bb::BlackboardEntry { "stop_robot_to_robot_infrared_broadcasting" } },    //
        { bb::entryKey(bluetoothSOC, dev::sensors, 0x2C), bb::BlackboardEntry { "robot_to_robot_infrared_message_received_notify" } },    //
        { bb::entryKey(nordic, dev::sensors, 0x30), bb::BlackboardEntry { "get_ambient_light_sensor_value" } },    //
        { bb::entryKey(bluetoothSOC, dev::sensors, 0x32), bb::BlackboardEntry { "stop_robot_to_robot_infrared_following" } },    //
        { bb::entryKey(bluetoothSOC, dev::sensors, 0x33), bb::BlackboardEntry { "start_robot_to_robot_infrared_evading" } },    //
        { bb::entryKey(bluetoothSOC, dev::sensors, 0x34), bb::BlackboardEntry { "stop_robot_to_robot_infrared_evading" } },    //
        //
        { bb::entryKey(nordic, dev::sensors, 0x35), bb::BlackboardEntry { "enable_color_detection_notify" } },    //
        { bb::entryKey(nordic, dev::sensors, 0x36), bb::BlackboardEntry { "color_detection_notify" } },    //
        { bb::entryKey(nordic, dev::sensors, 0x37), bb::BlackboardEntry { "get_current_detected_color_reading" } },    //
        { bb::entryKey(nordic, dev::sensors, 0x38), bb::BlackboardEntry { "enable_color_detection" } },    //
        //
        { bb::entryKey(bluetoothSOC, dev::sensors, 0x39), bb::BlackboardEntry { "configure_streaming_service" } },    //
        { bb::entryKey(nordic, dev::sensors, 0x39), bb::BlackboardEntry { "configure_streaming_service" } },    //
        { bb::entryKey(bluetoothSOC, dev::sensors, 0x3A), bb::BlackboardEntry { "start_streaming_service" } },    //
        { bb::entryKey(nordic, dev::sensors, 0x3A), bb::BlackboardEntry { "start_streaming_service" } },    //
        { bb::entryKey(bluetoothSOC, dev::sensors, 0x3B), bb::BlackboardEntry { "stop_streaming_service" } },    //
        { bb::entryKey(nordic, dev::sensors, 0x3B), bb::BlackboardEntry { "stop_streaming_service" } },    //
        { bb::entryKey(bluetoothSOC, dev::sensors, 0x3C), bb::BlackboardEntry { "clear_streaming_service" } },    //
        { bb::entryKey(nordic, dev::sensors, 0x3C), bb::BlackboardEntry { "clear_streaming_service" } },    //
        { bb::entryKey(bluetoothSOC, dev::sensors, 0x3D), bb::BlackboardEntry { "streaming_service_data_notify" } },    //
        { bb::entryKey(nordic, dev::sensors, 0x3D), bb::BlackboardEntry { "streaming_service_data_notify" } },    //
        //
        { bb::entryKey(nordic, dev::sensors, 0x3D, 0x03), bb::BlackboardEntry { "color stream" } },    //
        { bb::entryKey(nordic, dev::sensors, 0x3D, 0x05), bb::BlackboardEntry { "nordic core time lower" } },    //
        { bb::entryKey(nordic, dev::sensors, 0x3D, 0x09), bb::BlackboardEntry { "nordic core time upper" } },    //
        { bb::entryKey(nordic, dev::sensors, 0x3D, 0x0A), bb::BlackboardEntry { "ambient stream" } },    //
        //
        { bb::entryKey(bluetoothSOC, dev::sensors, 0x3D, 0x02), bb::BlackboardEntry { "accelerometer stream" } },          //
        { bb::entryKey(bluetoothSOC, dev::sensors, 0x3D, 0x05), bb::BlackboardEntry { "bt core time lower" } },            //
        { bb::entryKey(bluetoothSOC, dev::sensors, 0x3D, 0x09), bb::BlackboardEntry { "bt core time upper" } },            //
        { bb::entryKey(bluetoothSOC, dev::sensors, 0x3D, 0x04), bb::BlackboardEntry { "gyro stream" } },                   //
        { bb::entryKey(bluetoothSOC, dev::sensors, 0x3D, 0x01), bb::BlackboardEntry { "imu stream" } },                    //
        { bb::entryKey(bluetoothSOC, dev::sensors, 0x3D, 0x06), bb::BlackboardEntry { "locator stream" } },                //
        { bb::entryKey(bluetoothSOC, dev::sensors, 0x3D, 0x0B), bb::BlackboardEntry { "quat stream" } },                   //
        { bb::entryKey(bluetoothSOC, dev::sensors, 0x3D, 0x08), bb::BlackboardEntry { "speed stream" } },                  //
        { bb::entryKey(bluetoothSOC, dev::sensors, 0x3D, 0x07), bb::BlackboardEntry { "velocity stream" } },               //
        //
        { bb::entryKey(bluetoothSOC, dev::sensors, 0x3D, sen_token::imu_accel_gyro_token), bb::BlackboardEntry { "imu accel gyro stream" } },    //
        { bb::entryKey(bluetoothSOC, dev::sensors, 0x3D, sen_token::velocity_locator_speed_token), bb::BlackboardEntry {
            "velocity locator speed token" } },    //
        { bb::entryKey(bluetoothSOC, dev::sensors, 0x3D, sen_token::encoders_stream_token), bb::BlackboardEntry { "encoders token" } },    //
        //
        { bb::entryKey(bluetoothSOC, dev::sensors, 0x3E), bb::BlackboardEntry { "enable_robot_infrared_message_notify" } },    //
        { bb::entryKey(bluetoothSOC, dev::sensors, 0x3F), bb::BlackboardEntry { "send_infrared_message" } },    //
        //
        { bb::entryKey(bluetoothSOC, dev::sensors, 0x4A, (uint8_t)TemperatureIndexes::left_motor_temperature), bb::BlackboardEntry {
            "left_motor_temperature" } },    // left
        { bb::entryKey(bluetoothSOC, dev::sensors, 0x4A, (uint8_t)TemperatureIndexes::right_motor_temperature), bb::BlackboardEntry {
            "right_motor_temperature" } },    // right
        { bb::entryKey(bluetoothSOC, dev::sensors, 0x4A, (uint8_t)TemperatureIndexes::nordic_die_temperature), bb::BlackboardEntry {
            "nordic_die_temperature" } },    //
        { bb::entryKey(bluetoothSOC, dev::sensors, 0x4B), bb::BlackboardEntry { "get_motor_thermal_protection_status" } },    //
        { bb::entryKey(bluetoothSOC, dev::sensors, 0x4C), bb::BlackboardEntry { "enable_motor_thermal_protection_status_notify" } },    //
        { bb::entryKey(bluetoothSOC, dev::sensors, 0x4D), bb::BlackboardEntry { "motor_thermal_protection_status_notify" } },    //
        //
        { bb::entryKey(bluetoothSOC, dev::sensors, 0x51), bb::BlackboardEntry { "magnetometer_calibration_complete_notify" } },    //
        { bb::entryKey(bluetoothSOC, dev::sensors, 0x52), bb::BlackboardEntry { "get_magnetometer_reading" } },    //
        { bb::entryKey(bluetoothSOC, dev::sensors, 0x53), bb::BlackboardEntry { "get_encoder_counts" } },    //
        { bb::entryKey(bluetoothSOC, dev::sensors, 0x54), bb::BlackboardEntry { "disable_notifications_and_active_commands" } },    //

    };

}
