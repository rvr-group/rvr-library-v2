//======================================================================================================================
// 2021 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: rmerriam
//
//     Created: May 29, 2021
//
//======================================================================================================================
#include <Blackboard.h>
#include <PayloadDecode.h>
#include <ApiShell.h>
#include <Connection.h>
#include <Trace.h>
using namespace std::literals;
//----------------------------------------------------------------------------------------------------------------------
namespace rvr {

    ResultString Connection::name() {
        return decode_result<std::string>(mBlackboard.entryValue(mTarget, mDevice, get_bluetooth_advertising_name));
    }
//----------------------------------------------------------------------------------------------------------------------
    void ApiShell::echo(RvrMsg const& data) const {
        cmdData(echo_cmd, data, CommandResponse::resp_yes);
        cmdDataAlt(echo_cmd, data, CommandResponse::resp_yes);
    }
//----------------------------------------------------------------------------------------------------------------------
    ResultMsg ApiShell::echoBT() const {
        auto msg { mBlackboard.entryValue(mTarget, mDevice, echo_cmd) };
        ResultMsg res;
        if ( !msg.empty()) {
            res = msg;
        }
        return res;
    }
//----------------------------------------------------------------------------------------------------------------------
    ResultMsg ApiShell::echoNordic() const {
        auto msg { mBlackboard.entryValue(mAltTarget, mDevice, echo_cmd) };
        ResultMsg res;
        if ( !msg.empty()) {
            res = msg;
        }
        return res;
    }

    void ApiShell::generateApiError(uint8_t const err) const {
        reqByte(generate_api_error, err, CommandResponse::resp_yes);
        reqByteAlt(generate_api_error, err, CommandResponse::resp_yes);
    }

}
