//======================================================================================================================
// 2021 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: rmerriam
//
//     Created: May 29, 2021
//
//======================================================================================================================
#include <numeric>
#include <Trace.h>
#include <ReadPacket.h>
//----------------------------------------------------------------------------------------------------------------------
namespace rvr {
    //----------------------------------------------------------------------------------------------------------------------
    void ReadPacket::checkForData() {
        uint8_t r[buffer_size];
        int cnt = mStream.read(r, mInput.capacity());
        mInput.insert(mInput.end(), r, &r[cnt]);
    }
    //----------------------------------------------------------------------------------------------------------------------
    uint8_t ReadPacket::checksum() const {
        uint8_t sum {};
        sum = ~std::accumulate(mPacket.begin(), mPacket.end(), 0);
        return sum;
    }
    //----------------------------------------------------------------------------------------------------------------------
    void ReadPacket::processData() {
        static uint8_t const EopSop[] { EOP, SOP };

        auto pos = std::search(mInput.begin(), mInput.end(), EopSop, &EopSop[1]);
        mPacket.clear();

        if (pos != mInput.end()) {

            mPacket.insert(mPacket.begin(), mInput.begin(), pos + 1);
            mInput.erase(mInput.begin(), pos + 1);
            unescape();
            removeDelimiters();

            if (checksum() == 0) {
                mPacket.erase(mPacket.end() - 1);    // checksum
            }
            else {
                mPacket.clear();
            }
        }
    }
    //----------------------------------------------------------------------------------------------------------------------
    RvrMsg ReadPacket::read() {
        checkForData();
        processData();
        return mPacket;
    }
    //----------------------------------------------------------------------------------------------------------------------
    void ReadPacket::removeDelimiters() {
        mPacket.erase(mPacket.begin());    // SOP
        mPacket.erase(mPacket.end() - 1);    // EOP
    }
    //----------------------------------------------------------------------------------------------------------------------
    void ReadPacket::unescape() {
        for (auto p { find(mPacket.begin(), mPacket.end(), ESC) }; p != mPacket.end(); p = find(p + 1, mPacket.end(), ESC)) {
            unescape_char(p);
        }
    }
    //----------------------------------------------------------------------------------------------------------------------
    void ReadPacket::unescape_char(auto& p) {
        auto n { p + 1 };
        switch ( *n) {
            case escaped_SOP: {
                *n = SOP;
                break;
            }
            case escaped_EOP: {
                *n = EOP;
                break;
            }
            case escaped_ESC: {
                *n = ESC;
                break;
            }
        }
        mPacket.erase(p);
    }
}
