//======================================================================================================================
// 2021 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http:                                  //www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: rmerriam
//
//     Created: May 29, 2021
//
//======================================================================================================================
#include <thread>
#include <vector>

#include <Trace.h>

#include <Blackboard.h>
//----------------------------------------------------------------------------------------------------------------------
namespace rvr {
    using namespace std::literals;

    //----------------------------------------------------------------------------------------------------------------------
    rvr::Blackboard::key_t Blackboard::msgKey(TargetPort const proc, Devices const dev, uint8_t const cmd, uint8_t const seq) {

        Blackboard::key_t key;

        if(seq >= 0x80) {
            key = Blackboard::entryKey(proc, dev, cmd);
        }
        else {
            key = Blackboard::entryKey(proc, dev, cmd, seq);
        }
        return key;
    }
    //----------------------------------------------------------------------------------------------------------------------
    std::string Blackboard::entryName(key_t key) {
        std::string s;
        mys::TraceOff tout_ctrl { mys::tout };
        mys::tout << code_line << "key: " << std::hex << key << mys::sp << key_s(key) << std::dec;

        auto it { mDictionary.find(key) };

        if(it == mDictionary.end()) {
            it = mDictionary.find(key & 0xFFFFFF00);
            mys::tout << code_line << "key: " << std::hex << (key & 0xFFFFFF00);
        }
        if(it != mDictionary.end()) {
            s = it->second.name;
            mys::tout << code_line << "key: " << std::hex << key << mys::tab << s;
        }
        return s;
    }
    //----------------------------------------------------------------------------------------------------------------------
    void Blackboard::addEntryValue(key_t const key, RvrMsg value) {
        mAccessFlag.acquire();
        mDictionary[key].value = value;
        mAccessFlag.release();
    }
    //----------------------------------------------------------------------------------------------------------------------
    RvrMsg Blackboard::entryValue(key_t const key) const {
        RvrMsg res;
        mAccessFlag.acquire();
        if(auto it = mDictionary.find(key); (it != mDictionary.end()) & (!it->second.value.empty())) {
            res = RvrMsg { it->second.value };
        }

        mAccessFlag.release();
        return res;
    }
    //----------------------------------------------------------------------------------------------------------------------
    void Blackboard::async(key_t key, int16_t timeout) {
        uint16_t wait_time { 20 };

        timeout /= wait_time;    // calc number of loops
        while(entryValue(key).empty() && --timeout > 0) {
            std::this_thread::sleep_for(20ms);
        }
    }
    //----------------------------------------------------------------------------------------------------------------------
    void Blackboard::clearEntry(TargetPort const proc, Devices const dev, uint8_t const cmd) {
        addEntryValue(entryKey(proc, dev, cmd, 0), RvrMsg {});
    }
    //----------------------------------------------------------------------------------------------------------------------
    RvrMsg Blackboard::entryValue(TargetPort const proc, Devices const dev, uint8_t const cmd, uint8_t const id) const {
        auto msg { entryValue(entryKey(proc, dev, cmd, id)) };

        RvrMsg msg_opt { msg };
        return msg;
    }
    //======================================================================================================================
    //  Method to put RvrMsg data into dictionary
    //----------------------------------------------------------------------------------------------------------------------
    void Blackboard::msgArray(uint8_t const src, uint8_t const dev, uint8_t const cmd, bool const is_resp, RvrMsg::iterator begin,
        RvrMsg::iterator end) {
        mys::tdbg << code_entry;

        RvrMsg msg { begin, end };

        mys::tdbg << code_line << src << mys::sp    //
            << std::hex << dev << mys::sp    //
            << cmd << mys::sp    //
            << is_resp << mys::sp    //
            << msg;

        uint8_t id {};

        switch(msg[0]) {
            case 0xFF :    //  notifications (seq == 0xFF)
                if(msg.size() > 1) {    // notification with data
                    msg.erase(0, 1);    // remove 0xFF while leaving data
                }
                if(cmd == 0x3D) {      // streaming data - get stream tokey as id
                    id = msg[0];
                    msg.erase(0, 1);    // remove token
                }
                break;

            case 0x00 :    // battery volt in volts - seq is id field
            case 0x01 :
            case 0x02 :
                id = msg[0];
                msg.erase(0, 2);    // remove seq and error code
                break;

            case 0x04 :    // temperatures - seq is id field
            case 0x05 :
            case 0x08 :
                id = msg[0];
                msg.erase(0, 3);    // remove seq, error code, and id
                break;

            case 0x20 :    // enable commands
            case 0x21 :    // disable commands
                msg[0] = (msg[0] == 0x20);    // if seq is 20 is enable
                msg.erase(1, 1);
                break;

            default :    // general messages
                if(msg.size() > 2) {    // message with data
                    msg.erase(0, 2);
                }
                else {                 // simple response to command
                    msg.erase(0, 1);
                }
                break;
        }

        auto key = msgKey(TargetPort(src), Devices(dev), cmd, id);

        mys::tdbg << code_line << std::hex << '[' << key_s(key) << ']' << mys::sp << entryName(key) << mys::sp    //
            << msg;
        addEntryValue(key, msg);

        mys::tdbg << code_exit;
    }
    //======================================================================================================================
    std::ostream& operator <<(std::ostream& os, Blackboard::key_s const& key) {
        os << std::hex << std::uppercase << std::right;
        os << std::setfill('0') << std::setw(2) << (uint16_t)key.key.bits_v.proc + 1 << mys::sp    //
           << std::setw(2) << (uint16_t)key.key.bits_v.dev + 0x10 << mys::sp    //
           << std::setw(2) << (uint16_t)key.key.bits_v.cmd << mys::sp    //
           << std::setw(2) << (uint16_t)key.key.bits_v.id;
        return os;
    }
    //----------------------------------------------------------------------------------------------------------------------
    void Blackboard::dump() {
        struct v_map {
            Blackboard::key_t key;
            Blackboard::BlackboardEntry be;
        };

        mys::tdbg << code_entry;
        mys::tout << code_entry << mys::nl;

        std::vector <v_map> vec;
        for(auto b : rvr::Blackboard::mDictionary) {
            v_map v { b.first, b.second };
            vec.push_back(v);
        }

        std::sort(vec.begin(), vec.end(),                       //
                  [](v_map const& a, v_map const& b) {
                      return a.key < b.key;
                  }
        );

        for(auto& i : vec) {
            mys::tout << i.key << mys::sp << (key_s)i.key << mys::sp << std::setw(45) << std::setfill(' ') << std::left << i.be.name << mys::tab
                << i.be.value;
        }

        mys::tdbg << code_exit << mys::nl;
    }
}    // namespace rvr
