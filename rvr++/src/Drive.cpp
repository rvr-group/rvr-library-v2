//======================================================================================================================
// 2021 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: rmerriam
//
//     Created: May 29, 2021
//
//======================================================================================================================
#include <PayloadDecode.h>
#include <Drive.h>
//----------------------------------------------------------------------------------------------------------------------
namespace rvr {
    //----------------------------------------------------------------------------------------------------------------------
    auto int16_pack = [](int16_t const& value) -> RvrMsg {
        RvrMsg msg(2, 0);
        msg[0] = value >> 8;
        msg[1] = value & 0xFF;
        return msg;
    };
    //----------------------------------------------------------------------------------------------------------------------
    auto float_pack = [](float const& value) -> RvrMsg {
        RvrMsg msg(4, 0);

        union {
            uint8_t buf[4];
            float result { 0 };
        };
        result = value;

        msg[2] = buf[0];
        msg[3] = buf[1];
        msg[1] = buf[2];
        msg[0] = buf[3];

        return msg;
    };
    //======================================================================================================================
    Result <Drive::ControlSystemId> Drive::activeControlSystemId() const {
        auto msg { mBlackboard.entryValue(mTarget, mDevice, get_active_control_system_id) };
        return static_cast <Drive::ControlSystemId>(msg.empty() ? uint8_t {} : decode_type <uint8_t>(msg));
    }
    //----------------------------------------------------------------------------------------------------------------------
    ResultString Drive::activeControlSystemIdText() const {
        char const* ControlSystemIdText[] {    //
        "decelerating_stop",                 //
            "raw_motor",                 //
            "tank_drive",                //
            "drive_with_yaw_advanced_mode",              //
            "drive_with_yaw_basic_mode",                 //
            "rc_drive_rate_mode",                //
            "rc_drive_slew_mode",                //
            "xy_position_drive",                 //
            "infrared_follow_and_evade",                 //
            "magnetometer_calibration",              //
        };
        Drive::ControlSystemId id { activeControlSystemId().get_or() };
        return std::string { ControlSystemIdText[(uint8_t)id] };
    }
    //----------------------------------------------------------------------------------------------------------------------
    void Drive::getActiveControlSystemId(CommandResponse const want_resp) const {
        basic(get_active_control_system_id, want_resp);
    }
    //----------------------------------------------------------------------------------------------------------------------
    void Drive::restoreInitialDefaultControlSystems(CommandResponse const want_resp) const {
        basic(restore_initial_default_control_systems, want_resp);
    }
    //----------------------------------------------------------------------------------------------------------------------
    void Drive::setActiveControlSystemId(Drive::ControlSystemType const type, Drive::ControlSystemId const id,
        CommandResponse const want_resp) const {
        RvrMsg data { type, id };
        cmdData(set_default_control_system_for_type, data, want_resp);
    }
    //----------------------------------------------------------------------------------------------------------------------
    void Drive::getTypeDefaultControlSystem(Drive::ControlSystemType const type, CommandResponse const want_resp) const {
        reqByte(get_default_control_system_for_type, uint8_t(type), want_resp);
    }
    //======================================================================================================================
    void Drive::drive(double const& left, double const& right, CommandResponse const want_resp) const {
        auto [l_speed, l_mode] = speed_mode(left);
        auto [r_speed, r_mode] = speed_mode(right);

        RvrMsg msg { l_mode, l_speed, r_mode, r_speed };
        cmdData(raw_motors, msg, want_resp);
    }
    //----------------------------------------------------------------------------------------------------------------------
    void Drive::driveRcNormalized(    //
        int8_t const& yaw_angular_velocity, int8_t const& linear_velocity,    //
        uint8_t const& flags,    //
        CommandResponse const want_resp) const {
        RvrMsg msg { (uint8_t)yaw_angular_velocity, (uint8_t)linear_velocity, flags };
        cmdData(drive_rc_normalized, msg, want_resp);
    }
    //----------------------------------------------------------------------------------------------------------------------
    void Drive::driveTankNormalized(int8_t const& left, int8_t const& right, CommandResponse const want_resp) const {
        RvrMsg msg { (uint8_t)left, (uint8_t)right };
        cmdData(drive_tank_normalized, msg, want_resp);
    }
    //----------------------------------------------------------------------------------------------------------------------
    void Drive::driveToPositionNormalized(int16_t const& yaw_angle, float const& x, float const& y,    //
        uint8_t const& linear_velocity, ModeFlags const& flags,    //
        CommandResponse const want_resp) const {
        RvrMsg yaw { int16_pack(yaw_angle) };
        RvrMsg x_msg { float_pack(x) };
        RvrMsg y_msg { float_pack(y) };
        RvrMsg vel { uint8_t(linear_velocity) };
        RvrMsg flg { uint8_t(flags) };
        cmdData(drive_to_position_normalized, yaw + x_msg + y_msg + vel + flg, want_resp);
    }
    //----------------------------------------------------------------------------------------------------------------------
    void Drive::driveYawNormalized(int16_t const& yaw_angle, int8_t const& linear_velocity, CommandResponse const want_resp) const {
        RvrMsg yaw { int16_pack(yaw_angle) };
        RvrMsg vel { (uint8_t)linear_velocity };
        cmdData(drive_with_yaw_normalized, yaw + vel, want_resp);
    }
    //----------------------------------------------------------------------------------------------------------------------
    void Drive::driveRcSI(float const& yaw_angular_velocity, float const& linear_velocity, uint8_t const& flags,
        CommandResponse const want_resp) const {
        RvrMsg yaw { float_pack(yaw_angular_velocity) + float_pack(linear_velocity) };
        RvrMsg flg { flags };
        cmdData(drive_rc_si_units, yaw + flg, want_resp);
    }
    //----------------------------------------------------------------------------------------------------------------------
    void Drive::driveTankSI(float const& left, float const& right, CommandResponse const want_resp) const {
        RvrMsg l_r { float_pack(left) + float_pack(right) };
        cmdData(drive_tank_si_units, l_r, want_resp);
    }
    //----------------------------------------------------------------------------------------------------------------------
    void Drive::driveToPositionSI(    //
        float const& yaw_angle,    //
        float const& x, float const& y,    //
        float const& linear_velocity,    //
        ModeFlags const& flags,    //
        CommandResponse const want_resp) const {

        mBlackboard.clearEntry(mTarget, mDevice, xy_position_drive_result_notify);

        RvrMsg msg { float_pack(yaw_angle) + float_pack(x) + float_pack(y) + float_pack(linear_velocity) + (uint8_t)flags };
        cmdData(drive_to_position_si, msg, want_resp);
    }
    //----------------------------------------------------------------------------------------------------------------------
    void Drive::driveYawSI(float const& yaw_angle, float const& linear_velocity, CommandResponse const want_resp) const {
        RvrMsg msg { float_pack(yaw_angle) + float_pack(linear_velocity) };
        cmdData(drive_with_yaw_si, msg, want_resp);
    }
    //----------------------------------------------------------------------------------------------------------------------
    void Drive::driveWithHeading(double const& speed, int const& heading, CommandResponse const want_resp) const {
        auto [spd, mode] = speed_mode(abs(speed));
        RvrMsg msg { spd, static_cast <uint8_t>(heading >> 8), static_cast <uint8_t>(heading & 0xFF), static_cast <uint8_t>((speed >= 0.0) ? 0 : 1) };
        cmdData(drive_with_heading, msg, want_resp);
    }
    //----------------------------------------------------------------------------------------------------------------------
    void Drive::stop(CommandResponse const want_resp) const {
        basic(drive_stop, want_resp);
        mBlackboard.clearEntry(mTarget, mDevice, drive_stop);
    }
    //======================================================================================================================
    rvr::ResultBool Drive::motorFaultState() const {
        return decode_type <bool>(mBlackboard.entryValue(mTarget, mDevice, get_motor_fault_state));
    }
    //----------------------------------------------------------------------------------------------------------------------
    rvr::ResultBool Drive::motorFaultNotifySet() const {
        return decode_type <bool>(mBlackboard.entryValue(mTarget, mDevice, enable_motor_fault_notify));
    }
    //----------------------------------------------------------------------------------------------------------------------
    rvr::ResultBool Drive::motorStallNotifySet() const {
        return decode_type <bool>(mBlackboard.entryValue(mTarget, mDevice, enable_motor_stall_notify));
    }
    //----------------------------------------------------------------------------------------------------------------------
    void Drive::getMotorFault(CommandResponse const want_resp) const {
        basic(get_motor_fault_state, want_resp);
    }
    //----------------------------------------------------------------------------------------------------------------------
    void Drive::enableMotorFaultNotify(CommandResponse const want_resp) const {
        cmdEnable(enable_motor_fault_notify, want_resp);
    }
    //----------------------------------------------------------------------------------------------------------------------
    void Drive::disableMotorFaultNotify(CommandResponse const want_resp) const {
        cmdDisable(enable_motor_fault_notify, want_resp);
    }
    //----------------------------------------------------------------------------------------------------------------------
    void Drive::enableMotorStallNotify(CommandResponse const want_resp) const {
        cmdEnable(enable_motor_stall_notify, want_resp);
    }
    //----------------------------------------------------------------------------------------------------------------------
    void Drive::disableMotorStallNotify(CommandResponse const want_resp) const {
        cmdDisable(enable_motor_stall_notify, want_resp);
    }
    //======================================================================================================================
    std::tuple <unsigned char, unsigned char> const Drive::speed_mode(double const& speed) const {
        uint8_t s { static_cast <uint8_t>(abs(speed) / 100.0 * 256) };
        uint8_t m { static_cast <uint8_t>((speed > 0) ? forward : ((speed < 0) ? reverse : off)) };
        return std::tuple(s, m);
    }
    //----------------------------------------------------------------------------------------------------------------------
    void Drive::getStopControllerState(CommandResponse const want_resp) {
        basic(get_stop_controller_state, want_resp);
    }
    //----------------------------------------------------------------------------------------------------------------------
    void Drive::getDriveTargetSlewParameters(CommandResponse const want_resp) {
        basic(get_drive_target_slew_parameters, want_resp);
        mBlackboard.clearEntry(mTarget, mDevice, get_drive_target_slew_parameters);
    }
    //----------------------------------------------------------------------------------------------------------------------
    void Drive::restoreDefaultControlSystemTimeout(CommandResponse const want_resp) {
        basic(restore_default_control_system_timeout, want_resp);
    }
    //----------------------------------------------------------------------------------------------------------------------
    void Drive::restoreDefaultDriveTargetSlewParameters(CommandResponse const want_resp) {
        basic(restore_default_drive_target_slew_parameters, want_resp);
    }
    //----------------------------------------------------------------------------------------------------------------------
    Result <Drive::SlewRateParams> Drive::targetSlewParameters() const {
        auto const msg { mBlackboard.entryValue(mTarget, mDevice, get_drive_target_slew_parameters) };
        Result <Drive::SlewRateParams> res;

        if(!msg.empty()) {
            PayloadDecode <float, float, float, float, uint8_t> payload(msg);

            res = Drive::SlewRateParams {    //
            payload.get <0>(),    //
                payload.get <1>(),    //
                payload.get <2>(),    //
                payload.get <3>(),    //
                payload.get <4>(),    //
            };
        }
        return res;
    }
    //----------------------------------------------------------------------------------------------------------------------
    void Drive::setDriveTargetSlewParameters(SlewRateParams const& slew, CommandResponse const want_resp) {
        RvrMsg slew_msg { float_pack(slew.a) + float_pack(slew.b) + float_pack(slew.c) + float_pack(slew.linear_acceleration) + RvrMsg { slew.method } };
        cmdData(set_drive_target_slew_parameters, slew_msg, want_resp);
    }
    //----------------------------------------------------------------------------------------------------------------------
    ResultBool Drive::isStopControllerStopped() const {
        auto const msg { mBlackboard.entryValue(mTarget, mDevice, robot_has_stopped_notify) };
        return decode_result <bool>(msg);
    }
    //----------------------------------------------------------------------------------------------------------------------
    void Drive::setCustomControlSystemTimeout(float const& timeout, CommandResponse const want_resp) const {
        RvrMsg msg { int16_pack(timeout) };
        cmdData(set_custom_control_system_timeout, msg, want_resp);
    }
    //----------------------------------------------------------------------------------------------------------------------
    ResultBool Drive::isDriveToPositionDone() const {
        auto const msg { mBlackboard.entryValue(mTarget, mDevice, xy_position_drive_result_notify) };
        return decode_result <bool>(msg);
    }
    //----------------------------------------------------------------------------------------------------------------------
    void Drive::clearStopControllerStopped() {
        mBlackboard.clearEntry(mTarget, mDevice, robot_has_stopped_notify);
    }
}
