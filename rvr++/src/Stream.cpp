//======================================================================================================================
// 2021 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
#include <Stream.h>
#include <SerialPort.h>
#include <TcpClient.h>

#include <Trace.h>
//----------------------------------------------------------------------------------------------------------------------
namespace rvr {
   auto Stream::make_stream(int argc, char* argv[]) -> std::unique_ptr<rvr::Stream> {
      std::unique_ptr<rvr::Stream> stream_ptr;

      if (argc == 2) {
         stream_ptr = std::make_unique<SerialPort>(argv[1], 115200);
      }
      else {
         stream_ptr = std::make_unique<TcpClient>(argv[1], argv[2]);
      }

      return stream_ptr;
   }
}
