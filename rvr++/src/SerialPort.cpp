//======================================================================================================================
// 2010 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
#include <fcntl.h>
#include <termios.h>

#include <SerialPort.h>
//-----------------------------------------------------------------------------
namespace rvr {
   //-----------------------------------------------------------------------------
   SerialPort::SerialPort(std::string const& port_name, uint32_t const baud) {
      speed_t myBaud;

      switch (baud) {
         case 50:
            myBaud = B50;
            break;
         case 75:
            myBaud = B75;
            break;
         case 110:
            myBaud = B110;
            break;
         case 134:
            myBaud = B134;
            break;
         case 150:
            myBaud = B150;
            break;
         case 200:
            myBaud = B200;
            break;
         case 300:
            myBaud = B300;
            break;
         case 600:
            myBaud = B600;
            break;
         case 1200:
            myBaud = B1200;
            break;
         case 1800:
            myBaud = B1800;
            break;
         case 2400:
            myBaud = B2400;
            break;
         case 4800:
            myBaud = B4800;
            break;
         case 9600:
            myBaud = B9600;
            break;
         case 19200:
            myBaud = B19200;
            break;
         case 38400:
            myBaud = B38400;
            break;
         case 57600:
            myBaud = B57600;
            break;
         case 115200:
            myBaud = B115200;
            break;
         case 230400:
            myBaud = B230400;
            break;

         default:
            myBaud = B9600;
      }

      mCommId = ::open(port_name.c_str(), O_RDWR | O_NOCTTY);

      // Get and modify current options:
      struct termios options;
      tcgetattr(mCommId, &options);

      cfsetspeed(&options, myBaud);
      cfmakeraw(&options);

      options.c_cc[VMIN] = 0;
      options.c_cc[VTIME] = 50;

      if (tcsetattr(mCommId, TCSANOW | TCSAFLUSH, &options) == 0) {
         mIsOpen = true;
      }
   }
}
