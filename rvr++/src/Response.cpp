//======================================================================================================================
// 2021 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http:          //www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: rmerriam
//
//     Created: May 29, 2021
//
//======================================================================================================================
#include <thread>

#include <Trace.h>

#include <Blackboard.h>
#include <ReadPacket.h>
#include <Response.h>
#include <RvrData.h>
using namespace std::literals;

namespace rvr {

    using DeviceNames = std::unordered_map <uint8_t, std::string>;

    DeviceNames device_names {              //
    { 0x10, "api_and_shell" },              //
        { 0x11, "system" },              //
        { 0x13, "power" },              //
        { 0x16, "drive" },              //
        { 0x18, "sensors" },              //
        { 0x19, "connection" },              //
        { 0x1A, "io_led" },              //
    };
    //----------------------------------------------------------------------------------------------------------------------
    void Response::operator ()() {

        while ( !mShutdown) {
            rvr::RvrMsg packet { mReadPacket.read() };
            if ( !packet.empty()) {
                decode(packet);
            }
//            std::this_thread::sleep_for(0ms);
            std::this_thread::yield();
        }
    }

    void Response::shutdown() {
        mShutdown = true;
    }

    //----------------------------------------------------------------------------------------------------------------------
    void Response::decode_flags(uint8_t const f) {
        std::string flags {};

        if ((f & response) == 0) {
            flags += "notification | ";
        }

        for (auto mask { 0x01 }; mask != 0; mask <<= 1) {
            switch (mask & f) {
                case response:
                    flags += "response | ";
                    break;
                case request_response:
                    flags += "request_response | ";
                    break;
                case request_error_response:
                    flags += "request_error_response | ";
                    break;
                case activity:
                    flags += "activity | ";
                    break;
                case has_target:
                    flags += "has_target | ";
                    break;
                case has_source:
                    flags += "has_source | ";
                    break;
                case has_more_flags:
                    flags += "has_more_flags | ";
                    break;
            }
        }
    }
    //----------------------------------------------------------------------------------------------------------------------
    std::string Response::decode_error(auto err_byte) {
        static std::string const error_codes[11] {              //
        "Success",    //
            "bad_did",              //
            "bad_cid",              //
            "not_yet_implemented",              //
            //
            "cannot be executed in current mode",//
            "bad_data_length",              //
            "failed for command specific reason",              //
            "Bad Parameter Value",              //
            //
            "busy",//
            "bad_tid",              //
            "target_unavailable"              //
        };

        return error_codes[err_byte];
    }
    //----------------------------------------------------------------------------------------------------------------------
    void Response::decode(RvrMsg packet) {
        mys::tdbg << code_entry;

        // typical positions of header bytes when target not present which is the usual case
        uint8_t flags { 0x00 };
        //        uint8_t targ { 0x01 };             // usually not present
        uint8_t src { 0x01 };
        uint8_t dev { 0x02 };
        uint8_t cmd { 0x03 };
        uint8_t seq { 0x04 };
        uint8_t err_code { 0x05 };
        uint8_t data { 0x06 };

        bool const is_resp { (packet[flags] & response) == response };              // versus notification

        if (packet[flags] & has_target) {              //
            ++src;
            ++dev;
            ++cmd;
            ++seq;
            ++err_code;
            ++data;
        }

        decode_flags(packet[flags]);

        std::string device = device_names[packet[dev]];

        if (is_resp && packet[err_code]) {

            // a response will have a status of either 0 or an error code
            auto err_byte { packet[err_code] };
            mys::tdbg << code_line << "ERROR: " << (uint16_t)err_byte << mys::sp << decode_error(err_byte) << std::hex << mys::sp << packet;
        }
        else {
            mys::tdbg << code_line << std::right << std::hex << packet;
            mBlackboard.msgArray(packet[src], packet[dev], packet[cmd], is_resp, packet.begin() + seq, packet.end());
        }
        mys::tdbg << code_exit;
    }
}
