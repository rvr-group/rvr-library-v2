#pragma once
//======================================================================================================================
// 2021 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http:      //www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: rmerriam
//
//     Created: May 29, 2021
//
//======================================================================================================================
#include <Request.h>
#include <RvrData.h>
//----------------------------------------------------------------------------------------------------------------------
namespace rvr {
    class Blackboard;

    class Power : protected Request, public WaitFor<Power> {

    public:
        enum BatteryVoltState : uint8_t {
            unknown, ok, low, critical
        };

        enum MotorSide : uint8_t {
            left, right
        };

        enum VoltageType : uint8_t {
            CalibratedFiltered, CalibratedUnfiltered, UncalibratedUnfiltered,
        };

        Power(Blackboard& bb, SendPacket& req) :
            Request { bb, Devices::power, req, nordic } {
        }

        Power(Power const& other) = delete;
        Power(Power&& other) = delete;
        Power& operator=(Power const& other) = delete;
        Power& operator=(Power const&& other) = delete;

        //----------------------------------------------------------------------------------------------------------------------
        // reqeusts
        void powerOff(uint8_t const secs, CommandResponse const want_resp = CommandResponse::resp_on_error) const;
        void sleep(CommandResponse const want_resp = CommandResponse::resp_on_error) const;
        void awaken() const noexcept;    // must have response to hard coded

        void batteryPercentage(CommandResponse const want_resp = CommandResponse::resp_yes) const;
        void batteryVoltageState(CommandResponse const want_resp = CommandResponse::resp_yes) const;

        void enableBatteryStateChange(CommandResponse const want_resp = CommandResponse::resp_yes) const;
        void disableBatteryStateChange(CommandResponse const want_resp = CommandResponse::resp_yes) const;

        void batteryVoltage(VoltageType const vt, CommandResponse const want_resp = CommandResponse::resp_yes) const;
        void batteryVoltThresholds(CommandResponse const want_resp = CommandResponse::resp_yes) const;
        void batteryMotorCurrent(MotorSide const ms, CommandResponse const want_resp = CommandResponse::resp_yes) const;

        //----------------------------------------------------------------------------------------------------------------------
        // Data access methods

        ResultUInt8 batteryPercent() const noexcept;

        Result<Power::BatteryVoltState> voltState() const noexcept;
        ResultString voltStateText() const noexcept;

        // batteryVoltage
        ResultFloat voltsCalibratedFiltered() const noexcept;
        ResultFloat voltsCalibratedUnfiltered() const noexcept;
        ResultFloat voltsUncalibratedUnfiltered() const noexcept;

        // batteryVoltThresholds
        Result<VoltageThresholds> voltageThresholds() const noexcept;

        // batteryMotorCurrent
        ResultFloat motorCurrent(MotorSide const ms) const noexcept;

        ResultBool isAwake() const noexcept;    // check for awake() response
        ResultBool isWakeNotify() const noexcept;
        void resetWakeNotify() const noexcept;

        ResultBool isBatteryStateChangeEnabled() const noexcept;
        ResultBool isDidSleepNotify() const noexcept;
        void resetSleepNotify() const noexcept;

        ResultBool isWillSleepNotify() const noexcept;

    private:
        enum Cmd : uint8_t {
            power_off = 0x00,                                        //
            snooze = 0x01,                                        //
            wake = 0x0D,                                        //
            system_awake_notify = 0x11,                                        // ??
            will_sleep_notify = 0x19,                                        //
            did_sleep_notify = 0x1A,                                        //

            get_battery_percentage = 0x10,                                        //
            get_battery_voltage_state = 0x17,                                        //
            enable_battery_voltage_state_change_notify = 0x1B,                                        //
            battery_voltage_state_change_notify = 0x1C,                                        //
            get_battery_voltage_in_volts = 0x25,                                        //
            get_battery_voltage_state_thresholds = 0x26,                                        //
            get_current_sense_amplifier_current = 0x27,                                        //
        };
    };
    //----------------------------------------------------------------------------------------------------------------------
    inline void Power::awaken() const noexcept {
        basic(wake, CommandResponse::resp_yes);
    }
    //----------------------------------------------------------------------------------------------------------------------
    inline void Power::powerOff(uint8_t const secs, CommandResponse const want_resp) const {
        reqByte(power_off, secs, want_resp);
    }
    //----------------------------------------------------------------------------------------------------------------------
    inline void Power::sleep(CommandResponse const want_resp) const {
        basic(snooze, want_resp);
    }
    //----------------------------------------------------------------------------------------------------------------------
    inline void Power::batteryPercentage(CommandResponse const want_resp) const {
        basic(get_battery_percentage, want_resp);
    }
    //----------------------------------------------------------------------------------------------------------------------
    inline void Power::batteryVoltageState(CommandResponse const want_resp) const {
        basic(get_battery_voltage_state, want_resp);
    }
    //----------------------------------------------------------------------------------------------------------------------
    inline void Power::batteryVoltage(VoltageType const vt, CommandResponse const want_resp) const {
        byteId(get_battery_voltage_in_volts, vt, want_resp);
    }
    //----------------------------------------------------------------------------------------------------------------------
    inline void Power::batteryVoltThresholds(CommandResponse const want_resp) const {
        basic(get_battery_voltage_state_thresholds, want_resp);
    }
    //----------------------------------------------------------------------------------------------------------------------
    inline void Power::batteryMotorCurrent(MotorSide const ms, CommandResponse const want_resp) const {
        byteAltId(get_current_sense_amplifier_current, ms, want_resp);
    }
    //----------------------------------------------------------------------------------------------------------------------
    inline void Power::enableBatteryStateChange(CommandResponse const want_resp) const {
        cmdEnable(enable_battery_voltage_state_change_notify, want_resp);
    }
    //----------------------------------------------------------------------------------------------------------------------
    inline void Power::disableBatteryStateChange(CommandResponse const want_resp) const {
        cmdDisable(enable_battery_voltage_state_change_notify, want_resp);
    }

} /* namespace rvr */
