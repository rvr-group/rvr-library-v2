#ifndef SENDPACKET_H_
#define SENDPACKET_H_
//======================================================================================================================
// 2021 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: rmerriam
//
//     Created: May 29, 2021
//
//======================================================================================================================
//  Build a message to send to the RVR

#include <Packet.h>
#include <Stream.h>
//----------------------------------------------------------------------------------------------------------------------
namespace rvr {

    class SendPacket {
    public:
        SendPacket(Stream& s) :
            mStream { s } {
        }

        void send(RvrMsg const& p);

    private:
        uint8_t checksum(RvrMsg const& payload) const;
        static bool isPacketChar(uint8_t const c);

        auto escape_char(RvrMsg::iterator& p, RvrMsg& payload);
        void escape_msg(RvrMsg& payload);

        Stream& mStream;
    };
}    // namespace rvr

#endif /* SENDPACKET_H_ */
