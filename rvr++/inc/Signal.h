#pragma once
//======================================================================================================================
// 2021 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//		 File: Signal.h
//
//     Author: rmerriam
//
//    Created: Dec 24, 2021
//
//======================================================================================================================
#include <csignal>

#include <Result.h>
#include <Power.h>

#include <Trace.h>
//---------------------------------------------------------------------------------------------------------------------
namespace rvr {

    //---------------------------------------------------------------------------------------------------------------------
    class Signal {
    public:
        Signal( __sighandler_t ctrl_c_handler, __sighandler_t term_handler) {
            std::signal(SIGINT, ctrl_c_handler);
            std::signal(SIGTERM, term_handler);
            mys::tout << code_line << "signal setup\n";
        }

//        static void terminate(int signal) {
//            mys::tout << code_line << signal << " received\n";
//        }

        Signal(Signal const& other) = delete;
        Signal(Signal&& other) = delete;
        Signal& operator=(Signal const& other) = delete;
        Signal& operator=(Signal&& other) = delete;
    };

} /* namespace rvr */
