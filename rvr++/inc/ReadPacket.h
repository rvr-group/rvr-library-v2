#pragma once
//======================================================================================================================
// 2021 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: rmerriam
//
//     Created: May 29, 2021
//
//======================================================================================================================
#include <Packet.h>
#include <RvrData.h>
#include <Stream.h>
//----------------------------------------------------------------------------------------------------------------------
namespace rvr {


    class ReadPacket {
        static constexpr auto buffer_size {2048};
    public:
        ReadPacket(Stream& s) :
            mStream { s } {
            mInput.reserve(buffer_size);
        }

        RvrMsg read();

    private:
        uint8_t checksum() const;
        void checkForData();
        void processData();
        void removeDelimiters();
        void unescape_char(auto& p);
        void unescape();

        Stream& mStream;
        RvrMsg mInput;
        RvrMsg mPacket;
    };

}    // namespace rvr
