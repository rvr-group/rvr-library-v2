#pragma once
//======================================================================================================================
// 2021 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: Rudyard Merriam
//
//     Created: May 29, 2021
//
//======================================================================================================================
//
#include <Stream.h>
//----------------------------------------------------------------------------------------------------------------------
namespace rvr {
    //----------------------------------------------------------------------------------------------------------------------
    class SerialPort : public Stream {
    public:
        SerialPort(std::string const& port_name, uint32_t const baud);

        SerialPort(SerialPort const& other) = delete;
        SerialPort(Stream&& other) = delete;
        auto operator=(Stream const& other) -> SerialPort& = delete;
        auto operator=(Stream&& other) -> SerialPort& = delete;
    };
} // namespace rvr
