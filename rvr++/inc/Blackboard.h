#pragma once
//======================================================================================================================
// 2021 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: rmerriam
//
//     Created: May 29, 2021
//
//======================================================================================================================
#include <semaphore>
#include <unordered_map>

#include <Packet.h>
#include <RvrData.h>
//----------------------------------------------------------------------------------------------------------------------
namespace rvr {
    //----------------------------------------------------------------------------------------------------------------------
    class Blackboard {

    public:
        using key_t = uint16_t;

        Blackboard() {
            mAccessFlag.release();
        }

        Blackboard(Blackboard const& other) = delete;
        Blackboard(Blackboard&& other) = delete;
        Blackboard& operator=(Blackboard const& other) = delete;
        Blackboard& operator=(Blackboard const&& other) = delete;

        void async(key_t key, int16_t timeout = 200);
        void clearEntry(TargetPort const proc, Devices const dev, uint8_t const cmd);

        void dump();

        std::string entryName(key_t key);
        RvrMsg entryValue(TargetPort const proc, Devices const dev, uint8_t const cmd, uint8_t const id = 0) const;

        void msgArray(uint8_t const src, uint8_t const dev, uint8_t const cmd, bool const is_resp, RvrMsg::iterator begin, RvrMsg::iterator end);
        Blackboard::key_t msgKey(TargetPort const proc, Devices const dev, uint8_t const cmd, uint8_t const seq);

        mutable std::binary_semaphore mAccessFlag { 1 };

    private:

        /*=============================================================================================================
         *
         * Key stuff is gnarly because of using unorderedmap which uses a hash table
         * but has limited hashing capability. The key is "key_t" but is a combination
         * of target port (or proccessor), device, command, and id. That's not easy to deal with so
         * key_s is a struct with those fields with a conversion operator to key_t. That allows
         * constructing the key by fields and then converting it to the key_t.
         *
         * Id is another hack...
         * There are some messages that use a parameter to ask
         * for different information. But the response doesn't contain that parameter.
         * In those cases the parameter is plugged in as sequence number. The actual
         * sequence numbers are limited to > 0x80h so anything less is an Id.
         *
         */

        struct key_s {
            constexpr key_s() {
            }
            constexpr key_s(key_t const kt) {
                key.key_v = kt;
            }
            constexpr key_s(TargetPort const proc, Devices const dev, uint8_t const cmd, uint8_t const id = 0) {
                key.bits_v.proc = proc - 1;
                key.bits_v.id = id;
                key.bits_v.cmd = cmd;
                key.bits_v.dev = dev - 0x10;
            }
            constexpr operator key_t() {
                return key.key_v;
            }

            union key {
                struct bits {    // hack: id & dev must fit in byte, then cmd in byte
                    uint8_t cmd :7;
                    uint8_t proc :1;
                    uint8_t id :4;
                    uint8_t dev :4;
                } bits_v;

                key_t key_v;
            } key;
        };

        struct BlackboardEntry {
            std::string name;
            RvrMsg value {};
        };

        using BBDictionary = std::unordered_map<key_t, BlackboardEntry>;
        static BBDictionary mDictionary;

        void addEntryValue(key_t const key, RvrMsg value);
        RvrMsg entryValue(key_t const key) const;

        constexpr static key_t entryKey(TargetPort const proc, Devices const dev, uint8_t const cmd, uint8_t const id = 0) {
            key_s ks { proc, dev, cmd, id };
            key_t kt { ks };
            return kt;
        }

        friend class Request;
        friend std::ostream& operator<<(std::ostream& os, Blackboard::key_s const& k);
    };
//----------------------------------------------------------------------------------------------------------------------
    std::ostream& operator<<(std::ostream& os, Blackboard::key_s const& k);

} /* namespace rvr */

