#pragma once
//======================================================================================================================
// 2021 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
#include <sys/ioctl.h>
#include <unistd.h>

#include <memory>
#include <string>
//----------------------------------------------------------------------------------------------------------------------
namespace rvr {
   using ustring = std::basic_string<uint8_t>;
   //----------------------------------------------------------------------------------------------------------------------
   class Stream {
   public:
      Stream() = default;
      virtual ~Stream() {
         ::close(mCommId);
      }

      Stream(Stream const& other) = default;
      Stream(Stream&& other) = default;
      auto operator=(Stream const& other) -> Stream& = delete;
      auto operator=(Stream&& other) -> Stream& = delete;
      //----------------------------------------------------------------------------------------------------------------------
      explicit operator bool() const {
         return mIsOpen;
      }
      //----------------------------------------------------------------------------------------------------------------------
      auto read(uint8_t buffer[], uint32_t const len) const -> int64_t {
         return ::read(mCommId, buffer, len);
      }
      //----------------------------------------------------------------------------------------------------------------------
      [[nodiscard]] auto read() const -> uint8_t {
         uint8_t ch{};

         if (::read(mCommId, &ch, 1) < 0) {
            return -1;
         }
         return ch;
      }
      //----------------------------------------------------------------------------------------------------------------------
      auto write(ustring const& msg) const {
         return ::write(mCommId, msg.c_str(), msg.size());
      }
      //----------------------------------------------------------------------------------------------------------------------
      [[nodiscard]] auto write(uint8_t const& ch) const -> int64_t {
         return ::write(mCommId, &ch, 1);
      }
      //----------------------------------------------------------------------------------------------------------------------
      [[nodiscard]] auto count() const -> int {
         int bytes_avail;
         ::ioctl(mCommId, FIONREAD, &bytes_avail);
         return bytes_avail;
      }
      //----------------------------------------------------------------------------------------------------------------------
      static auto make_stream(int argc, char* argv[]) -> std::unique_ptr<Stream>;

   protected:
      // set by derived class
      int mCommId{-1};
      bool mIsOpen{false};
   };
} /* namespace rvr */
