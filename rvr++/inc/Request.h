#pragma once
//======================================================================================================================
// 2021 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: rmerriam
//
//     Created: May 29, 2021
//
//======================================================================================================================
#include <chrono>
#include <functional>
#include <limits>
#include <thread>

//#include <Blackboard.h>
#include <Packet.h>
#include <SendPacket.h>

#include <utility.h>
//----------------------------------------------------------------------------------------------------------------------
namespace rvr {

    class Blackboard;

    //----------------------------------------------------------------------------------------------------------------------
    // Structure to provide aysnc waiting for reception of a response
    // It uses the CRTP to insert itself into all the classes derived from Request
    template <typename DerivedT>
    struct WaitFor {
        void wait_for(auto member, int16_t timeout = 100) {
            auto& derived = *static_cast <DerivedT*>(this);
            uint16_t wait_time { 20 };

            timeout /= wait_time;    // calc number of loops
            while((derived.*member)().invalid() && --timeout > 0) {
                std::this_thread::sleep_for(std::chrono::milliseconds(wait_time));
            }
        }
    };
    //----------------------------------------------------------------------------------------------------------------------
    enum struct CommandResponse : uint8_t {
        resp_no = no_response,       //
            resp_yes = request_response,    //
            resp_on_error = request_error_response | request_response,
    };
    //----------------------------------------------------------------------------------------------------------------------
    class Request {
    public:

        explicit Request(Blackboard& bb, Devices const device, SendPacket& request, TargetPort const target);

        Request(Request const& other) = delete;
        Request(Request&& other) = delete;
        Request& operator=(Request const& other) = delete;
        Request& operator=(Request const&& other) = delete;

        void basic(uint8_t const cmd, CommandResponse const want_resp = CommandResponse::resp_on_error) const;
        void basicAlt(uint8_t const cmd, CommandResponse const want_resp = CommandResponse::resp_on_error) const;

        void reqByte(uint8_t const cmd, uint8_t const data, CommandResponse const want_resp = CommandResponse::resp_on_error) const;
        void reqByteAlt(uint8_t const cmd, uint8_t const data, CommandResponse const want_resp = CommandResponse::resp_on_error) const;

        void byteId(uint8_t const cmd, uint8_t const data, CommandResponse const want_resp = CommandResponse::resp_on_error) const;
        void byteAltId(uint8_t const cmd, uint8_t const data, CommandResponse const want_resp = CommandResponse::resp_on_error) const;

        void reqInt(uint8_t const cmd, uint16_t const data, CommandResponse const want_resp = CommandResponse::resp_on_error) const;
        void reqIntAlt(uint8_t const cmd, uint16_t const data, CommandResponse const want_resp = CommandResponse::resp_on_error) const;

        void cmdEnable(uint8_t const cmd, CommandResponse const want_resp = CommandResponse::resp_on_error) const;
        void cmdEnableAlt(uint8_t const cmd, CommandResponse const want_resp = CommandResponse::resp_on_error) const;
        void cmdDisable(uint8_t const cmd, CommandResponse const want_resp = CommandResponse::resp_on_error) const;
        void cmdDisableAlt(uint8_t const cmd, CommandResponse const want_resp = CommandResponse::resp_on_error) const;

        void cmdData(uint8_t const cmd, RvrMsgView const& data, CommandResponse const want_resp = CommandResponse::resp_on_error) const;
        void cmdDataAlt(uint8_t const cmd, RvrMsg const& data, CommandResponse const want_resp = CommandResponse::resp_on_error) const;

    protected:
        uint8_t buildFlags(CommandResponse const want_resp) const;

        /*
         * NOTE: HACK ALERT!!!
         *
         * The sequence field is used for debugging only. It isn't used for
         * tracking messages. It is used as a special field for some messages that pass
         * an ID. The ID is put in the sequence field. During deserialization any
         * sequence less than 0x80 is used as part of the dictionary key for updating
         * values in the dictionary.
         *
         * A sequence value of 0xFF is an async notification generated
         * automatically by the RVR, as opposed to a response to a request.
         *
         * NOTE: while sequence() can generate 0xFF it doesn't cause a problem because
         * only commands with 0x3D are notifications with an 0xFF sequence.
         *
         */
        static uint8_t sequence() {
            return ++mSeq | 0x80;
        }

        Blackboard& mBlackboard;
        Devices const mDevice;
        SendPacket& mRequest;
        TargetPort const mTarget;
        TargetPort const mAltTarget;

    private:
        uint8_t makeAltTarget();

        static inline uint8_t mSeq { 0x80 };

    };
} /* namespace rvr */
