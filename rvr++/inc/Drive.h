#pragma once
//======================================================================================================================
// 2021 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: rmerriam
//
//     Created: May 29, 2021
//
//======================================================================================================================
#include <Blackboard.h>
#include <Request.h>
#include <Result.h>
//---------------------------------------------------------------------------------------------------------------------
namespace rvr {

    //---------------------------------------------------------------------------------------------------------------------
    class Drive : protected Request, public WaitFor <Drive> {

    public:
        enum ControlSystemType : uint8_t {
            control_system_type_stop = 0,    //
                control_system_type_raw_motor = 1,    //
                control_system_type_tank_drive = 2,     //
                control_system_type_drive_with_yaw = 3,     //
                control_system_type_rc_drive = 4,    //
                control_system_type_xy_position_drive = 5,    //
                control_system_type_infrared_drive = 6,     //
                control_system_type_magnetometer_drive = 7    //
        };

        enum ControlSystemId : uint8_t {
            decelerating_stop = 0,    //
                raw_motor = 1,    //
                tank_drive = 2,    //
                drive_with_yaw_advanced_mode = 3,    //
                drive_with_yaw_basic_mode = 4,    //
                rc_drive_rate_mode = 5,    //
                rc_drive_slew_mode = 6,    //
                xy_position_drive = 7,    //
                infrared_follow_and_evade = 8,    //
                magnetometer_calibration = 9
        };

        enum LinearVelocitySlewMethods : uint8_t {
            constant = 0,    //
                proportional = 1    //
        };

        enum class ModeFlags : uint8_t {
            forward = 0,    //
                reverse = 1,    //
                auto_direction = 2,    //
        };

        struct SlewRateParams {
            float a {};
            float b {};
            float c {};
            float linear_acceleration {};
            uint8_t method {};
        };
        //---------------------------------------------------------------------------------------------------------------------
        Drive(Blackboard& bb, SendPacket& req) :
            Request { bb, Devices::drive, req, bluetoothSOC } {
        }

        Drive(Drive&& other) = delete;
        Drive(Drive const& other) = delete;
        Drive(Drive const&& other) = delete;
        Drive& operator=(Drive const& other) = delete;
        Drive& operator=(Drive const&& other) = delete;

        void async(key_t key, int16_t timeout = 200) {
            mBlackboard.async(key, timeout);
        }
        //----------------------------------------------------------------------------------------------------------------------
        // Control system handline
        void getActiveControlSystemId(CommandResponse const want_resp = CommandResponse::resp_yes) const;
        void getTypeDefaultControlSystem(        //
            Drive::ControlSystemType const type,    //
            CommandResponse const want_resp = CommandResponse::resp_yes) const;
        void restoreInitialDefaultControlSystems(CommandResponse const want_resp = CommandResponse::resp_on_error) const;
        void setActiveControlSystemId(    //
            ControlSystemType const type, ControlSystemId const id,    //
            CommandResponse const want_resp = CommandResponse::resp_on_error) const;
        void setCustomControlSystemTimeout(float const& timeout, CommandResponse const want_resp = CommandResponse::resp_on_error) const;

        [[nodiscard]] Result <ControlSystemId> activeControlSystemId() const;
        [[nodiscard]] ResultString activeControlSystemIdText() const;
        //----------------------------------------------------------------------------------------------------------------------
        // motor fault handling
        void enableMotorStallNotify(CommandResponse const want_resp = CommandResponse::resp_on_error) const;
        void disableMotorStallNotify(CommandResponse const want_resp = CommandResponse::resp_on_error) const;

        void enableMotorFaultNotify(CommandResponse const want_resp = CommandResponse::resp_on_error) const;
        void disableMotorFaultNotify(CommandResponse const want_resp = CommandResponse::resp_on_error) const;

        void getMotorFault(CommandResponse const want_resp = CommandResponse::resp_on_error) const;

        [[nodiscard]] ResultBool motorFaultState() const;
        [[nodiscard]] ResultBool motorFaultNotifySet() const;
        [[nodiscard]] ResultBool motorStallNotifySet() const;

        //----------------------------------------------------------------------------------------------------------------------
        void getStopControllerState(CommandResponse const want_resp = CommandResponse::resp_on_error);
        void restoreDefaultControlSystemTimeout(CommandResponse const want_resp = CommandResponse::resp_on_error);
        ResultBool isStopControllerStopped() const;
        //----------------------------------------------------------------------------------------------------------------------
        // slew control
        void getDriveTargetSlewParameters(CommandResponse const want_resp = CommandResponse::resp_on_error);
        void restoreDefaultDriveTargetSlewParameters(CommandResponse const want_resp = CommandResponse::resp_on_error);
        void setDriveTargetSlewParameters(SlewRateParams const& slew, CommandResponse const want_resp = CommandResponse::resp_on_error);
        [[nodiscard]] Result <SlewRateParams> targetSlewParameters() const;
        //----------------------------------------------------------------------------------------------------------------------
        // drive
        void drive(        //
            double const& left, double const& right,    //
            CommandResponse const want_resp = CommandResponse::resp_on_error) const;

        void driveRcNormalized(    //
            int8_t const& yaw_angular_velocity, int8_t const& linear_velocity,    //
            uint8_t const& flags,    //
            CommandResponse const want_resp = CommandResponse::resp_on_error) const;

        void driveTankNormalized(    //
            int8_t const& left, int8_t const& right,    //
            CommandResponse const want_resp = CommandResponse::resp_on_error) const;

        void driveToPositionNormalized(int16_t const& yaw_angle,    //
            float const& x, float const& y,    //
            uint8_t const& linear_velocity,    //
            ModeFlags const& flags,    //
            CommandResponse const want_resp = CommandResponse::resp_on_error) const;

        void driveYawNormalized(    //
            int16_t const& yaw_angle,    //
            int8_t const& linear_velocity,    //
            CommandResponse const want_resp = CommandResponse::resp_on_error) const;

        void driveRcSI(    //
            float const& yaw_angular_velocity, float const& linear_velocity,    //
            uint8_t const& flags = 0,    //
            CommandResponse const want_resp = CommandResponse::resp_on_error) const;

        void driveTankSI(    //
            float const& left, float const& right,    //
            CommandResponse const want_resp = CommandResponse::resp_on_error) const;

        void driveToPositionSI(    //
            float const& yaw_angle,    //
            float const& x, float const& y,    //
            float const& linear_velocity,    //
            ModeFlags const& flags,    //
            CommandResponse const want_resp = CommandResponse::resp_on_error) const;

        void driveYawSI(    //
            float const& yaw_angle,    //
            float const& linear_velocity,    //
            CommandResponse const want_resp = CommandResponse::resp_on_error) const;

        void driveWithHeading(    //
            double const& speed,    //
            int const& heading,    //
            CommandResponse const want_resp = CommandResponse::resp_on_error) const;

        void stop(CommandResponse const want_resp = CommandResponse::resp_on_error) const;
        void clearStopControllerStopped();

        auto resetYaw(CommandResponse const want_resp = CommandResponse::resp_on_error) const;

        [[nodiscard]] ResultBool isDriveToPositionDone() const;

    private:

        // drive raw modes
        enum Mode : uint8_t {
            off, forward, reverse,
        };

        enum Cmd : uint8_t {
            raw_motors = 0x01,    //  v1
                reset_yaw = 0x06,    //  v1

                drive_with_heading = 0x07,    //  v1

                enable_motor_stall_notify = 0x25,    //  v1
                motor_stall_notify = 0x26,        //  v1
                enable_motor_fault_notify = 0x27,    //  v1
                motor_fault_notify = 0x28,        //  v1
                get_motor_fault_state = 0x29,     //  v1

                drive_tank_si_units = 0x32,    //+
                drive_tank_normalized = 0x33,    //+

                drive_rc_si_units = 0x34,    //+
                drive_rc_normalized = 0x35,    //+

                drive_with_yaw_si = 0x36,         //+
                drive_with_yaw_normalized = 0x37,    //+

                drive_to_position_si = 0x38,            //
                drive_to_position_normalized = 0x39,    //
                xy_position_drive_result_notify = 0x3A,    //

                set_drive_target_slew_parameters = 0x3C,    //+
                get_drive_target_slew_parameters = 0x3D,    //+

                drive_stop_custom_decel = 0x3E,                      //
                robot_has_stopped_notify = 0x3F,                     //+ get_stop_controller_state
                restore_default_drive_target_slew_parameters = 0x40,    //+
                get_stop_controller_state = 0x41,                    //+
                drive_stop = 0x42,                                   //+
                restore_default_control_system_timeout = 0x43,       //+

                set_default_control_system_for_type = 0x0E,    //+
                set_custom_control_system_timeout = 0x22,    //+
                get_active_control_system_id = 0x44,                 //+
                restore_initial_default_control_systems = 0x45,      //+
                get_default_control_system_for_type = 0x46           //+
        };
        std::tuple <uint8_t, uint8_t> const speed_mode(double const& speed) const;
    };
    //======================================================================================================================
    inline auto Drive::resetYaw(CommandResponse const want_resp) const {
        return basic(reset_yaw, want_resp);
    }
}    // namespace rvr

